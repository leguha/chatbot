<?php
header('Content-Type: application/json');

include_once 'JsonResponse.php';
include_once 'db_config.php';

$json = new JsonResponse();

$message = trim($_REQUEST['mess']);
if (!empty($message)) {
    $message = mysqli_real_escape_string($db, $message);
    $query = $db->query("SELECT * FROM `chat` WHERE `REQUEST` = '$message'");

    $knowResponse = rand(0, 100);

    $json->flag = $query->num_rows > 0 && $knowResponse > 10;
    if ($json->flag) {
        $response = [];
        $sumRating = 0;
        while ($ar = $query->fetch_array()) {
            $response[] = $ar;
            $sumRating += $ar["RATING"];
        }

        $rnd = rand(1, $sumRating);
        $id = 0;
        $start = 0;
        foreach ($response as $key => $item) {
//            $start = $key - 1 === -1 ? 0 : (int)$response[$key - 1]["RATING"]; 
            if ($start > $rnd && $rnd < $start + $item["RATING"]) {
                $id = $key;
                break;
            }
            $start += $item["RATING"];
        }

        $json->id = (int)$response[$id]["ID"];
        $json->text = $response[$id]["RESPONSE"];
    } else {
        $json->text = 'А что бы ты хотел услышать?';
        $json->response = $message;
        $json->id = 0;
    }
}
elseif (!empty($_REQUEST['request']) && !empty($_REQUEST['response'])) {
    $response = mysqli_real_escape_string($db, trim($_REQUEST['response']));
    $request = mysqli_real_escape_string($db, trim($_REQUEST['request']));
    $db->query("INSERT INTO `chat` (`REQUEST`, `RESPONSE`) VALUES ('$request', '$response')");

    $json->id = 0;
    $json->text = 'Хорошо)';
    $json->flag = true;
} elseif (!empty($_REQUEST['id'])) {
    $id = intval($_REQUEST['id']);
    if ($id > 0) {
        if ($_REQUEST['isLike'] === '1')
            $db->query("UPDATE `chat` SET `chat`.`RATING` = `chat`.`RATING` + 1 WHERE `chat`.`ID` = $id");
        else
            $db->query("UPDATE `chat` SET `chat`.`RATING` = `chat`.`RATING` - 1 WHERE `chat`.`ID` = $id");

        $query = $db->query("SELECT * FROM `chat` WHERE `chat`.`ID` = $id");
        $response = $query->fetch_array()[0];

        if ((int)$response < 1) {
            $db->query("DELETE FROM `chat` WHERE `chat`.`ID` = $id");
        }
    }
} elseif(empty($message)) {
    $json->text = 'Ты хотел что то спросить?';
    $json->id = 0;
    $json->flag = true;
}

if (get_class($json) === 'JsonResponse')
    echo json_encode($json,  JSON_UNESCAPED_UNICODE);