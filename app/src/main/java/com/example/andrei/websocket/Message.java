package com.example.andrei.websocket;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Message {
    private String text;
    private boolean flag;
    private String response;
    private int id;

    @JsonIgnore
    private boolean belongsToCurrentUser;
    @JsonIgnore
    private boolean rated;


    public Message(String text, boolean flag, boolean belongsToCurrentUser) {
        this.text = text;
        this.flag = flag;
        this.rated = false;
        this.belongsToCurrentUser = belongsToCurrentUser;
    }

    public boolean isBelongsToCurrentUser() {
        return belongsToCurrentUser;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isRated() {
        return rated;
    }

    public void setRated(boolean rated) {
        this.rated = rated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
