package com.example.andrei.websocket;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.List;

public class MessageAdapter extends BaseAdapter {

    List<Message> messages;
    Context context;

    public MessageAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    public void add(Message message) {
        this.messages.add(message);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Message getItem(int i) {
        return messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final MessageViewHolder holder = new MessageViewHolder();
        LayoutInflater messageInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        final Message message = messages.get(i);

        if (message.isBelongsToCurrentUser()) {
            convertView = messageInflater.inflate(R.layout.my_bubble, null);
            holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);
            convertView.setTag(holder);
            holder.messageBody.setText(message.getText());
        } else if(message.isBelongsToCurrentUser() == false && message.isRated() == false && message.getId() != 0){
            convertView = messageInflater.inflate(R.layout.their_bubble, null);
            holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);

            holder.like = (ImageButton) convertView.findViewById(R.id.like);
            holder.dislike = (ImageButton) convertView.findViewById(R.id.dislike);

            holder.like.setVisibility(View.VISIBLE);
            holder.dislike.setVisibility(View.VISIBLE);

            holder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AndroidNetworking.post("http://chatbot.izweb.ru/api.php")
                            .addBodyParameter("id", String.valueOf(message.getId()))
                            .addBodyParameter("isLike", "1")
                            .build()
                            .getAsObject(Message.class, new ParsedRequestListener<Message>() {
                                @Override
                                public void onResponse(Message mess) {
                                    Toast.makeText(context, "Спасибо за оценку! Я становлюсь умнее!", Toast.LENGTH_SHORT).show();
                                    holder.like.setVisibility(View.GONE);
                                    holder.dislike.setVisibility(View.GONE);
                                    message.setRated(true);
                                }
                                @Override
                                public void onError(ANError anError) {
                                    Toast.makeText(context, "Произошла какая-то ошибка!", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            });

            holder.dislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AndroidNetworking.post("http://chatbot.izweb.ru/api.php")
                            .addBodyParameter("id", String.valueOf(message.getId()))
                            .addBodyParameter("isLike", "0")
                            .build()
                            .getAsObject(Message.class, new ParsedRequestListener<Message>() {
                                @Override
                                public void onResponse(Message mess) {
                                    Toast.makeText(context, "Спасибо за оценку! Я становлюсь умнее!", Toast.LENGTH_SHORT).show();
                                    holder.like.setVisibility(View.GONE);
                                    holder.dislike.setVisibility(View.GONE);
                                    message.setRated(true);
                                }
                                @Override
                                public void onError(ANError anError) {
                                    Toast.makeText(context, "Произошла какая-то ошибка!", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            });

            convertView.setTag(holder);
            holder.messageBody.setText(message.getText());
        } else {
            convertView = messageInflater.inflate(R.layout.their_bubble, null);
            holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);
            convertView.setTag(holder);
            holder.messageBody.setText(message.getText());
        }

        return convertView;
    }

}

class MessageViewHolder {
    public TextView messageBody;
    public ImageButton like;
    public ImageButton dislike;
}