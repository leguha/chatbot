package com.example.andrei.websocket;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    EditText messageText;
    ImageButton submitButton;
    ListView listView;

    MessageAdapter adapter;

    List<Message> messages = new ArrayList<Message>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Message mes1 = new Message("Привет", true, true);
        Message mes2 = new Message("Привет)", true, false);

        messages.add(mes1);
        messages.add(mes2);

        listView = (ListView) findViewById(R.id.list);
        messageText = (EditText) findViewById(R.id.messageText);
        submitButton = (ImageButton) findViewById(R.id.submit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String message = messageText.getText().toString();

                adapter.add(new Message(message, true, true));
                listView.smoothScrollToPosition(adapter.getCount() - 1);

                messageText.setText("");
                Message lastMessage = adapter.getItem(adapter.getCount() - 2);

                if(lastMessage.isFlag()){
                    AndroidNetworking.post("http://chatbot.izweb.ru/api.php")
                            .addBodyParameter("mess", message)
                            .build()
                            .getAsObject(Message.class, new ParsedRequestListener<Message>() {
                                @Override
                                public void onResponse(Message mess) {
                                    adapter.add(mess);
                                    listView.smoothScrollToPosition(adapter.getCount() - 1);
                                }
                                @Override
                                public void onError(ANError anError) {
                                    Toast.makeText(MainActivity.this, "Произошла какая-то ошибка!", Toast.LENGTH_SHORT).show();
                                }
                            });
                } else {

                    AndroidNetworking.post("http://chatbot.izweb.ru/api.php")
                            .addBodyParameter("request", adapter.getItem(adapter.getCount() - 2).getResponse())
                            .addBodyParameter("response", message)
                            .build()
                            .getAsObject(Message.class, new ParsedRequestListener<Message>() {
                                @Override
                                public void onResponse(Message mess) {
                                    adapter.add(mess);
                                    listView.smoothScrollToPosition(adapter.getCount() - 1);
                                }
                                @Override
                                public void onError(ANError anError) {
                                    Toast.makeText(MainActivity.this, "Произошла какая-то ошибка!", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });

        adapter = new MessageAdapter(this, messages);
        listView.setAdapter(adapter);

    }

}

